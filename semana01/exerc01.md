# Curso Básico de Programação do Bash

## Sugestões para melhor aproveitamento

- Crie um repositório no codeberg.org com o nome `cbps-estudos`.
- Nele, publique suas anotações, respostas e scritpts.
- Ainda nele, crie um arquivo `notas.md` para justificar e estender suas respostas.
- Se quiser, compartilhe o link e peça uma avaliação nas nossas issues.

## Entendeu? Então me explica...

Escolha as alternativas corretas (se houver alguma).

### 1. O que o shell não é?

- [ ] Uma interface.
- [ ] Uma linguagem de programação.
- [x] Um compilador de programas.
- [ ] Um interpretador de comandos.

### 2. Qual é o shell padrão na maioria dos sistemas GNU/Linux?

- [ ] ash
- [ ] dash
- [x] bash
- [x] sh

### 3. Onde está o erro na pergunta anterior?

- [x] Todos esses shells podem ser encontrados no GNU/Linux.
- [x] O shell `sh` é do UNIX.
- [x] Depende do que você quer dizer com "padrão".
- [ ] Os shells `ash` e `dash` são POSIX.

### 4. O sistema operacional gerencia programas em execução...

- [ ] Através da memória.
- [ ] Pela linha de comandos.
- [x] Através de processos.
- [ ] Pelo programa `top`.

### 5. O período de execução do shell define...

- [ ] Um script.
- [ ] Um programa.
- [ ] Um processo.
- [x] Uma sessão.

### 6. Uma nova sessão do shell pode ser inciada...

- [x] Pela abertura de um terminal.
- [ ] Por *pipes* e agrupamentos com parêntesis.
- [x] Pela invocação do executável do shell.
- [x] Pela execução de um script.

### 7. No Bash, todos os comandos em uma pipeline...

- [x] São executados na sessão mãe.
- [ ] São executados em um subshell.
- [x] Recebem uma cópia do ambiente da sessão mãe.
- [x] São encadeados condicionalmente.

### 8. Qual é a ordem correta de processamento de uma linha de comando?

1. Redirecionamentos.
2. Separação da linha em palavras e operadores.
3. Monitoramento do estado de saída.
4. Classificação de comandos.
5. Expansões.

- [ ] 3 2 5 1 4
- [x] 2 4 5 1 3
- [ ] 1 2 3 5 4
- [ ] 4 2 1 3 5

### 9. Em que etapa de processamento os apelidos são expandidos?

- [ ] Terceira.
- [x] Segunda.
- [ ] Primeira.
- [ ] Quarta.

### 10. Quais operadores de controle não estão definidos corretamente?

- [ ] `&&` e `||`: encadeamento condicional.
- [ ] `|` e `|&`: encadeamento por pipe.
- [x] `&` e `;`: execução assíncrona (segundo plano).
- [ ] `;` e `nova linha`: separadores de linhas de comando.

### 11. Quais são as afirmativas incorretas?

- [x] Parâmetros são os dados associados a uma sessão do shell.
- [ ] Variáveis são parâmetros identificados por *nomes válidos*.
- [ ] Nomes válidos podem ter letras da tabela ASCII, sublinhado e números.
- [ ] Somente o shell pode criar e alterar variáveis.

### 12. Quais dos nomes abaixo são inválidos para variáveis?

- [x] `endereço`
- [ ] `turma1`
- [ ] `_`
- [ ] `__`

### 13. No shell, variáveis podem ser acessadas...

- [x] Por expansão.
- [x] Por referência.
- [x] Por valor.
- [ ] Variáveis só podem ser expandidas ou referenciadas.

### 14. No shell, variáveis não são declaradas porque...

- [ ] Não precisa.
- [ ] Todos os dados são do tipo indeterminado.
- [x] Tudo é *string*.
- [ ] O shell não reserva regiões na memória em função do tipo.

### 15. Quais comandos do Bash não definem, alteram atributos e exibem variáveis?

- [ ] `declare`
- [ ] `typeset`
- [x] `local`
- [ ] `export`

### 16. Quais são as afirmativas corretas?

- [x] Em princípio, todas as variáveis são globais na sessão.
- [x] O termo "ambiente" inclui todas as variáveis de uma sessão.
- [x] Variáveis definidas pelo comando `declare` em funções são locais.
- [x] Variáveis são exportadas para a sessão mãe com o comando `export`.

### 17. Quantas palavras existem no comando abaixo?

```
:~$ echo 'Maria tinha' um carneirinho.
```

- [ ] 5
- [ ] 4
- [x] 3
- [ ] 2

### 18. Qual seria a saída do comando abaixo?

```
:~$ var=duas palavras
```

- [x] `bash: palavras: comando não encontrado`
- [ ] `bash: duas: comando não encontrado`
- [ ] `bash: var=duas: comando não encontrado`
- [ ] Nenhuma saída.

### 19. Como eu poderia definir a variável 'nome' para obter o resultado abaixo?

```
:~$ echo "Seja bem-vindo, $nome."
Seja bem-vindo, João da Silva.
```

- [ ] `nome='João da Silva'`
- [ ] `read nome`
- [x] `nome=João da Silva`
- [ ] `nome="$1"`

### 20. Explique o comportamento abaixo:

```
# Causa erro...
:~$ nome=João da Silva
bash: da: comando não encontrado

# Não causa erro...
:~$ pessoa='João da Silva'
:~$ nome=$pessoa
:~$
```

- [ ] Porque os apelidos são expandidos antes das variáveis.
- [ ] Porque as aspas só são removidas ao fim das expansões.
- [ ] Mentira! Também daria o mesmo erro!
- [x] Porque as aspas removem o significado especial dos espaços.

## Bora praticar!

### Nosso primeiro script

- [x] Com qualquer editor de textos planos, crie um arquivo com o nome 'salve.sh'.
- [x] No arquivo, inclua na primeira linha: `#!/usr/bin/env bash`.
- [x] Numa linha seguinte qualquer, escreva um comando para exibir a mensagem `Salve, simpatia!`.
- [x] Salve e dê permissão de execução para o arquivo.
- [x] Na pasta onde o script foi criado, execute-o com `./salve.sh`.

### Nosso segundo script

- [x] Copie o arquivo anterior com o nome `boas.sh`.
- [x] Antes do comando, atribua o seu nome a uma variável chamada `nome`.
- [x] Altere o comando para expandir `nome` onde está a palavra `simpatia`.

### Continue experimentando

- [x] Altere `boas.sh` para que o seu nome seja solicitado.
- [x] Agora, o nome exibido deverá ser aquele que você digitar.
- [x] Use sua criatividade para fazer o script solicitar e imprimir outras informações.
